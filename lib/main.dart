import 'package:boso_przez_hotel/views/zarezerwuj.dart';
import 'package:flutter/material.dart';
import 'views/home.dart';
import 'views/loading.dart';
import 'views/dodaj_rezerwacje.dart';

void main() => runApp(MaterialApp(

  //initialRoute: '/home',
  routes: {
    '/': (context) => Loading(),
    '/home': (context) => Home(),
    '/dodajRezerwacje' : (context) => DodajRezerwacje(),
    '/zarezerwuj' : (context) => Zarezerwuj()
  },

    theme: ThemeData(
      // Define the default brightness and colors.
      //brightness: Brightness.light,
      primaryColor: Color(0xff4FB3FF),
      accentColor: Color(0xfffc5130),

      )

));

