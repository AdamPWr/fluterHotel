import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;


class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {


  String login = "m";
  String password = "baczek123";
  String token;

  // Otrzymanie tokenu dostępu do systemu
  // Token dostajesz w response.body
  void tryLogin () async {

    var url = 'http://vps694183.ovh.net:8000/api-token-auth';

    var response = await http.post(url, body: {'username': "$login", 'password': "$password"});

    print(login);
    print(password);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if(response.statusCode == 200)
      {
        Map data = jsonDecode(response.body);
        token = data["token"];
        print('token w loadingu $token');
        Navigator.pushReplacementNamed(context, "/home", arguments: {'token' : token});
      }
    else
      {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(
              title: new Text("Błędne dane logowania"),
//              content: new Text(access_code),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text("Close"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );


        print("Cos sie popsulo");
      }

  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(15.0),
          decoration: BoxDecoration(
            color: Colors.grey[300]
          ),

          child: Column(

            mainAxisAlignment: MainAxisAlignment.center,

            children: <Widget>[
              Text(
                'Hotel "Na Projekcie z Baz"', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
              ),
              SizedBox(
                height: 30.0,
              ),
              CircleAvatar(
                backgroundImage: AssetImage('images/logo.png'), //NetworkImage("http://4.s.dziennik.pl/pliki/3958000/3958202-wojciech-cejrowski.jpg"),
                radius: 70,
              ),
              SizedBox(
                height: 10.0,
              ),
              TextField(
                obscureText: false,
                onSubmitted: (String value){ login = value;},
                onChanged: (String value){ login = value;},
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Login',
                ),
              ),
              SizedBox(height: 20,),
              TextField(
                obscureText: true,
                onSubmitted: (String value){ password = value;},
                onChanged: (String value){ password = value;},
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                ),
              ),
              RaisedButton(
                onPressed: tryLogin,
                textColor: Colors.grey[200],
                color: Theme.of(context).primaryColor,
                child: Text("Zaloguj")
              )
            ],
          ),
        )
        ),
      );

  }
}


