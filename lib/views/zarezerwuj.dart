import 'dart:convert';
import 'dart:io';

import 'package:intl/intl.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class Zarezerwuj extends StatefulWidget {
  @override
  _ZarezerwujState createState() => _ZarezerwujState();
}

class _ZarezerwujState extends State<Zarezerwuj> {
  final format = DateFormat("yyyy-MM-dd");

  Map data;
  String token;
  List reservation_set;
  int room_id;

  String start_date;
  String end_date;

  Map httpResponse;

  void zarezerwuj() async {
    var url = "http://vps694183.ovh.net:8000/hotel-api/make-reservation/";

    var response = await http.post(url, headers: {
      HttpHeaders.authorizationHeader: 'Token $token'
    }, body: {
      'date_start': start_date.substring(0, 10),
      'date_end': end_date.substring(0, 10),
      'room_id': room_id.toString(),
      'alimentation': '1'
    });

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    print("dupa");

    if (response.statusCode == 200) {
      httpResponse = jsonDecode(response.body);
    } else {
      print("Cos sie popsulo w home/getMyReservations()");
    }

    print(httpResponse);

    setState(() {});

   Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    data = ModalRoute.of(context).settings.arguments;
    token = data['token'];
    reservation_set = data['reservation_set'];
    room_id = data['room_id'];

    print("Zarezerwuj build");
    print(token);
    print(reservation_set);
    print("Room id ${room_id}");

    return Scaffold(
      appBar: AppBar(
        title: Text("Rezerwuj"),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              ListTile(
                leading: Icon(Icons.hotel),
                title: Text(
                    'Rezerwacja pokoju nr ${room_id}'),
                subtitle: Text(
                  'Wybierz czas rezerwacji'
                ),
              ),


              SizedBox(
                height: 20.0,
              ),

              DateTimeField(
                format: format,
                decoration:
                    InputDecoration(labelText: 'Od', fillColor: Colors.grey),
                onChanged: (date) {
                  start_date = date.toString().substring(0, 16);
                  print(start_date);
                },
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      context: context,
                      firstDate: DateTime(1900),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2100));
                },
              ),
              DateTimeField(
                format: format,
                decoration:
                    InputDecoration(labelText: 'Do', fillColor: Colors.grey),
                onChanged: (date) {
                  end_date = date.toString().substring(0, 16);
                  print(start_date);
                },
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      context: context,
                      firstDate: DateTime(1900),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2100));
                },
              ),
              SizedBox(
                height: 20.0,
              ),
              RaisedButton(
                onPressed: zarezerwuj,
                color: Theme.of(context).primaryColor,
                child: const Text(
                  'Zatwierdź',
                  style: TextStyle(fontSize: 20),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
