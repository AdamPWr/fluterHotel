import 'dart:convert';
import 'dart:io';

import 'package:intl/intl.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:crypto/crypto.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Map data;
  List httpResponse;
  String token;
  var formatter = new DateFormat('yyyy-MM-dd');


  void payForReservation(reservation_id) async {
    var url = "http://vps694183.ovh.net:8000/hotel-api/pay-for-reservation/";

    var response = await http
        .post(url, body: {'reservation_id': reservation_id.toString()}, headers: {HttpHeaders.authorizationHeader: 'Token $token'});

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    print("dupa");


    print(httpResponse);

    await getMyReservations();

  }

  // PObranie listy pokojów
  void getMyReservations() async {
    await print("token w lini 25 w home to $token");

    var url = "http://vps694183.ovh.net:8000/hotel-api/my-reservations/";

    var response = await http
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Token $token'});

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    print("dupa");

    if (response.statusCode == 200) {
      httpResponse = jsonDecode(response.body);
    } else {
      print("Cos sie popsulo w home/getMyReservations()");
    }

    print(httpResponse);

    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    token = "dummy token";
    httpResponse = new List();
    getMyReservations();
    //print("token to $token");
  }

  @override
  Widget build(BuildContext context) {
    print("duPA1");
    data = ModalRoute.of(context).settings.arguments;
    token = data['token'];

    getMyReservations();

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Twoje rezerwacje",
          style: TextStyle(color: Colors.white, letterSpacing: 1),
        ),
        //backgroundColor: Colors.grey[900],
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
                padding: const EdgeInsets.all(15),
                itemCount: httpResponse.length,
                itemBuilder: (BuildContext context, int index) {
                  return Center(
                    child: Card(
                      child: Column(mainAxisSize: MainAxisSize.min, children: <
                          Widget>[
                        ListTile(
                          leading: Icon(Icons.hotel),
                          title: Text(
                              'Pokój nr  ${httpResponse[index]['room']['room_id']}'),
                          subtitle: Text(
                              '${httpResponse[index]['start_date'].substring(0, 10)} - ${httpResponse[index]['end_date'].substring(0, 10)}'),
                        ),
                        ButtonBar(
                          children: <Widget>[
                            FlatButton(
                              child: httpResponse[index]['paid'] == false
                                  ? Text(
                                      'OPŁAĆ (${httpResponse[index]['full_cost']} PLN)',
                                      style: TextStyle(color: Colors.green))
                                  : Text('OPŁACONO',
                                      style: TextStyle(color: Colors.green)),
                              onPressed: () {


                                payForReservation(httpResponse[index]['reservation_id']);
                                print(httpResponse[index]['reservation_id']);
                              },
                            ),
                            FlatButton(
                                child: const Text('KOD DOSTĘPU'),
                                onPressed: () {
                                  String access_code = md5
                                      .convert(utf8.encode(httpResponse[index]
                                              ['reservation_id']
                                          .toString()))
                                      .toString()
                                      .substring(0, 5);

                                  if( httpResponse[index]
                                  ['paid'] == false) {
                                    access_code = 'Musisz wpierw opłacić rezerwację!';
                                  }

                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      // return object of type Dialog
                                      return AlertDialog(
                                        title: new Text("Kod dostępu"),
                                        content: new Text(access_code),
                                        actions: <Widget>[
                                          // usually buttons at the bottom of the dialog
                                          new FlatButton(
                                            child: new Text("Close"),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  );

//                                      httpResponse[index]['paid'] == true
//                                          ? null
//                                          : null
                                }),
                          ],
                        ),
                      ]),
                    ),
                  );
                  /*
                  return Card(
                   // height: 100,

                    color: Colors.blue[350],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Od ${httpResponse[index]['start_date'].substring(0,10)}'),
                        Text('Do ${httpResponse[index]['end_date'].substring(0,10)}'),
                        Text('Ilosc lozek  ${httpResponse[index]['room']['equipment']['num_of_beds']}'),
                        Text('Koszt  ${httpResponse[index]['full_cost']}'),
                        Text('Numer pokoju  ${httpResponse[index]['full_cost']}'),
                        //Text('Koszt ${httpResponse[index]}'),
                      ],
                    ),
                  );
                  */
                }),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/dodajRezerwacje',
              arguments: {'token': token});
        },
        child: Icon(Icons.add),
        backgroundColor: Theme.of(context).accentColor,
      ),
    );
  }
}

// formatter.format(httpResponse[index]['start_date'])
