import 'dart:convert';
import 'dart:io';

import 'package:intl/intl.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class DodajRezerwacje extends StatefulWidget {
  @override
  _DodajRezerwacjeState createState() => _DodajRezerwacjeState();
}

class _DodajRezerwacjeState extends State<DodajRezerwacje> {
  String token;
  Map data;
  List httpResponse;

  List<String> alimentations = ['aa', 'bb', 'cc'];
  String dropdownVal = "aa";
  final format = DateFormat("yyyy-MM-dd");

  List pokoje;

  void getAlimenatatins() async {
    await print("token w lini 21 w Dodaj to $token");

    var url = "http://vps694183.ovh.net:8000/hotel-api/alimentations";

    var response = await http
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Token $token'});

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    print("dupa");

    if (response.statusCode == 200) {
      httpResponse = jsonDecode(response.body);
    } else {
      print("Cos sie popsulo w DodajRezerwacje/getAliemntations");
    }

    print(httpResponse);

    setState(() {});
  }

  void getPokoje() async {
    await print("token w lini 59 getPOkoje w Dodaj to $token");

    var url = "http://vps694183.ovh.net:8000/hotel-api/rooms";

    var response = await http
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Token $token'});

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    print("dupa");

    if (response.statusCode == 200) {
      httpResponse = jsonDecode(response.body);
    } else {
      print("Cos sie popsulo w DodajRezerwacje/getAliemntations");
    }

    print(httpResponse);

    setState(() {});
  }

  String Status() {
    return "Wolny";
  }

  void initState() {
    // TODO: implement initState
    super.initState();
    token = "dummy token";
    data = new Map();
    //httpResponse = new List();
    //getMyReservations();

    print("token w Dodawaniu  to $token");
    //getAlimenatatins();
    getPokoje();
  }

  @override
  Widget build(BuildContext context) {
    data = ModalRoute.of(context).settings.arguments;
    token = data['token'];

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.amber, //change your color here
        ),
        title: Text(
          "Dodaj rezerwacje",
          style: TextStyle(color: Colors.amber[500], letterSpacing: 2),
        ),
        centerTitle: true,
        backgroundColor: Colors.grey[900],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
                padding: const EdgeInsets.all(15),
                itemCount: httpResponse.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    // height: 100,

                    color: Colors.blue[350],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ListTile(
                          title: Text(
                              'Numer pokoju ${httpResponse[index]['room_id']}'),
                          leading: Icon(Icons.arrow_right),
                        ),
                        ListTile(
                          title: Text(
                              'Ilosc lozek  ${httpResponse[index]['equipment']['num_of_beds']}'),
                          leading: Icon(Icons.hotel),
                        ),
                        ListTile(
                          title: Text("Zarezerwuj"),
                          onTap: () {
                            Navigator.pushNamed(context, '/zarezerwuj',
                                arguments: {
                                  'token': token,
                                  'room_id': httpResponse[index]['room_id'],
                                  'reservation_set': httpResponse[index]
                                      ['reservation_set']
                                });
                          },
                        ),
                        //Text('Koszt ${httpResponse[index]}'),
                      ],
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
